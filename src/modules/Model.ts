

export type ModelId = {
    id: number
}

export type BaseModel = ModelId & {
    created_at: string
    updated_at: string
}