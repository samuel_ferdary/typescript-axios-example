import axios from 'axios';
import { ModelId } from './Model';

/**
 * Blueprint for HttpWrapper.
 */
export type HttpClient<Fields, Dto, Model> = {
    list: () => Promise<Model[]>
    item: (id: number) => Promise<Model|undefined>
    create: (payload: Fields) => Promise<ModelId|undefined>
    update: (id: number, payload: Dto) => Promise<Model|undefined>
    destroy: (id: number) => Promise<ModelId|undefined>
}


/**
 * Example token, please use config i.o hard coded.
 */
const token = '12345';


/**
 * Instantiate new http client with Axios.
 */
export const httpClient = axios.create({
    url: 'api.samfe.nl/',
    headers: {
        'Authorization': `Bearer ${token}`,
        Accept: 'application/json'
    }
});

