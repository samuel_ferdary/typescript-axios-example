import { ModelId } from "./modules/Model";
import { ProductModel, productResolver } from "./resources/product";


/**
 * Please use dynamic variable i.o. hard coded.
 */
const id: number = 1;


/**
 * List all products
 */
let productModels: ProductModel[] = [];
productResolver().list().then(list => {
    console.log({list});
    productModels = list;
});


/**
 * Get single product,
 */
let productModel: ProductModel|undefined = undefined;
productResolver().item(id).then(item => {
    console.log({item});
    productModel = item;
});


/**
 * Get ID of newly created product.
 */
let productCreatedId: ModelId|undefined = undefined;
productResolver().create({
    name: "Samfe swag",
    price: 9.95
})
.then(created => {
    console.log({created});
    productCreatedId = created;
});


/**
 * Get updated product model.
 */
let productUpdatedModel: ProductModel|undefined = undefined;
productResolver().update(id, {
    price: 9.95,
    color: 'Blue'
})
.then(updated => {
    console.log({updated});
    productUpdatedModel = updated;
});


/**
 * Get ID of deleted product.
 */
let productDestroyedId: ModelId|undefined = undefined;
productResolver().destroy(id)
.then(destroyed => {
    console.log({destroyed});
    productDestroyedId = destroyed;
});
