import { BaseModel, ModelId } from "../modules/Model"
import { HttpClient, httpClient } from "../modules/httpClient"


/**
 * Field values for POST request.
 */
export type ProductFields = {
    name: string
    price: number
    color?: string // Optional
}


/**
 * Data Transfer Object for PUT requests.
 * All fields are optional.
 */
export type ProductDto = Partial<ProductFields>


/**
 * Response from Back end.
 */
export type ProductModel = BaseModel & ProductDto


/**
 * Wrapper for product CRUD.
 */
export const productResolver = (): HttpClient<ProductFields, ProductDto, ProductModel> => {
    

    /**
     * Get list of products.
     * 
     * @returns {Promise<ProductModel[]>}
     */
    const list = async(): Promise<ProductModel[]> => await httpClient.get('products', {
        params: {
            // Optional parameters
            limit: 100
        }
    })
    .then(res =>  res.data ?? [])
    .catch(e => {
        console.error(e);
        return [];
    })
    .finally(() => {
        // Any action after resolved/reject
    });


    /**
     * Get single product.
     * 
     * @param {number} id 
     * @returns {Promise<ProductModel|undefined>}
     */
    const item = async (id:number): Promise<ProductModel|undefined> => await httpClient
        .get(`products/${id}`, {
            // Optional parameters
            params: {
                filter: 'name'
            }
        })
        .then(res => res.status == 200 ? res.data : undefined)
        .catch(e => {
            console.error(e);
            return undefined;
        })
        .finally(() => {
            // Any action after resolved/reject
        });


    /**
     * Create new product.
     * 
     * @param {ProductFields} payload 
     * @returns {Promise<ModelId|undefined>}
     */
    const create = async(payload: ProductFields): Promise<ModelId|undefined> => await httpClient
        .post('products', payload)
        .then(res => res.status == 201 ? res.data : undefined)
        .catch(e => {
            console.error(e);
            return undefined;
        })
        .finally(() => {
            // Any action after resolved/reject
        });


    /**
     * Update existing product.
     * 
     * @param {number} id 
     * @param {ProductDto} payload 
     * @returns {Promise<ProductModel|undefined>}
     */
    const update = async(id:number, payload: ProductDto): Promise<ProductModel|undefined> => await httpClient
        .put(`products/${id}`, payload)
        .then(res => res.status == 200||res.status == 201 ? res.data : undefined)
        .catch(e => {
            console.error(e);
            return undefined;
        })
        .finally(() => {
            // Any action after resolved/reject
        });


    /**
     * Delete single product.
     * 
     * @param {number} id 
     * @returns {Promise<ModelId|undefined>}
     */
    const destroy = async(id:number): Promise<ModelId|undefined> => await httpClient
        .delete(`products/${id}`)
        .then(res => res.status == 200 ? res.data : undefined)
        .catch(e => {
            console.error(e);
            return undefined;
        })
        .finally(() => {
            // Any action after resolved/reject
        });


    return {
        list,
        item,
        create,
        update,
        destroy
    }
}
